Projects
--------

1. https://github.com/IliasPap/COVIDNet

Datasets
--------

1. https://github.com/ieee8023/covid-chestxray-dataset/issues/4

References
----------

1. [Around 35:40 in podcast](https://librelounge.org/episodes/34-the-limits-of-the-agpl.html)
2. http://penta.debconf.org/dc12_schedule/attachments/238_dc12_ml.pdf

Ethics and Politics
-------------------

1. https://blog.jwf.io/2020/04/fosdem-2020-pt-2-can-free-software-include-ethical-ai-systems/
2. https://meta.wikimedia.org/wiki/EU_policy/Consultation_on_the_White_Paper_on_Artificial_Intelligence_(2020)
